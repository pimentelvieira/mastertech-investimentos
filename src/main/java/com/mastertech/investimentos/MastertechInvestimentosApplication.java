package com.mastertech.investimentos;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MastertechInvestimentosApplication {

	public static void main(String[] args) {
		SpringApplication.run(MastertechInvestimentosApplication.class, args);
	}

}
