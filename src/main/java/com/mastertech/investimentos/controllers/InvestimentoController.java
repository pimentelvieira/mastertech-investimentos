package com.mastertech.investimentos.controllers;

import com.mastertech.investimentos.models.Investimento;
import com.mastertech.investimentos.models.ResultadoSimulacao;
import com.mastertech.investimentos.models.Simulacao;
import com.mastertech.investimentos.services.InvestimentoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/investimentos")
public class InvestimentoController {

    @Autowired
    private InvestimentoService investimentoService;

    @PostMapping
    public ResponseEntity<Investimento> inserirInvestimento(@RequestBody @Valid Investimento investimento) {
        Investimento investimentoCriado = this.investimentoService.inserir(investimento);
        return ResponseEntity.status(HttpStatus.CREATED).body(investimentoCriado);
    }

    @PutMapping("/{id}")
    public ResponseEntity<Investimento> atualizarInvestimento(@RequestBody @Valid Investimento investimento, @PathVariable Integer id) {
        Optional<Investimento> investimentoBuscado = this.investimentoService.buscarPorId(id);

        if(investimentoBuscado.isPresent()) {
            investimento.setId(id);
            return ResponseEntity.status(HttpStatus.OK).body(this.investimentoService.atualizar(investimento));
        } else {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Investimento não encontrado");
        }
    }

    @DeleteMapping("/{id}")
    public void deletarInvestimento(@PathVariable Integer id) {
        Optional<Investimento> investimento = this.investimentoService.buscarPorId(id);
        if(investimento.isPresent()) {
            this.investimentoService.deletar(id);
        } else {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Investimento não encontrado");
        }
    }

    @GetMapping("/{id}")
    public Investimento buscarPorId(@PathVariable Integer id) {
        Optional<Investimento> investimento = this.investimentoService.buscarPorId(id);
        if(investimento.isPresent()) {
            return investimento.get();
        } else {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Investimento não encontrado");
        }
    }

    @GetMapping
    public List<Investimento> buscarTodos() {
        return this.investimentoService.buscarTodos();
    }

    @PutMapping("/simulacao")
    public ResultadoSimulacao simularInvestimento(@RequestBody @Valid Simulacao simulacao) {
        Optional<Investimento> investimento = this.investimentoService.buscarPorId(simulacao.getInvestimentoId());
        if(investimento.isPresent()) {
            return this.investimentoService.simularInvestimento(simulacao, investimento.get());
        } else {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Investimento não encontrado");
        }
    }
}
