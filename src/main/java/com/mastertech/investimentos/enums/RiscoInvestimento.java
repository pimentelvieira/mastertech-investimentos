package com.mastertech.investimentos.enums;

public enum RiscoInvestimento {
    BAIXO,
    MEDIO,
    ALTO
}
