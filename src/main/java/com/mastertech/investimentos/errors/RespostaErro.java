package com.mastertech.investimentos.errors;

import java.util.List;

public class RespostaErro {

    private String tipoErro;
    private int codigo;
    private String status;
    private String nomeObjeto;
    private List<ObjetoErro> erros;

    public RespostaErro(String tipoErro, int codigo, String status, String nomeObjeto, List<ObjetoErro> erros) {
        this.tipoErro = tipoErro;
        this.codigo = codigo;
        this.status = status;
        this.nomeObjeto = nomeObjeto;
        this.erros = erros;
    }

    public String getTipoErro() {
        return tipoErro;
    }

    public void setTipoErro(String tipoErro) {
        this.tipoErro = tipoErro;
    }

    public int getCodigo() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getNomeObjeto() {
        return nomeObjeto;
    }

    public void setNomeObjeto(String nomeObjeto) {
        this.nomeObjeto = nomeObjeto;
    }

    public List<ObjetoErro> getErros() {
        return erros;
    }

    public void setErros(List<ObjetoErro> erros) {
        this.erros = erros;
    }
}
