package com.mastertech.investimentos.exceptions;

import com.mastertech.investimentos.errors.ObjetoErro;
import com.mastertech.investimentos.errors.RespostaErro;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.util.List;
import java.util.stream.Collectors;

@RestControllerAdvice
public class ManipuladorExcecao extends ResponseEntityExceptionHandler {

    @Override
    protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {
        return ResponseEntity.status(status).body(getRespostaErro(ex, status, getErros(ex)));
    }

    private List<ObjetoErro> getErros(MethodArgumentNotValidException exception) {
        List<ObjetoErro> result = exception.getBindingResult().getFieldErrors()
                .stream().map(erro -> new ObjetoErro(erro.getDefaultMessage(),
                        erro.getField(),
                        erro.getRejectedValue())).collect(Collectors.toList());

        return result;
    }

    private RespostaErro getRespostaErro(MethodArgumentNotValidException exception, HttpStatus httpStatus, List<ObjetoErro> objetoErros) {
        RespostaErro result = new RespostaErro("Erro de validação",
                httpStatus.value(),
                httpStatus.getReasonPhrase(),
                exception.getBindingResult().getObjectName(),
                objetoErros);
        return result;
    }
}
