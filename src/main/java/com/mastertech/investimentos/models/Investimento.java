package com.mastertech.investimentos.models;

import com.mastertech.investimentos.enums.RiscoInvestimento;

import javax.validation.constraints.*;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Investimento {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @NotNull(message = "O campo nome não pode ser nulo")
    private String nome;

    @NotNull(message = "O campo descrição não pode ser nulo")
    private String descricao;

    @NotNull(message = "O campo risco não pode ser nulo")
    private RiscoInvestimento risco;

    @NotNull(message = "O campo porcentagemLucro não pode ser nulo")
    @DecimalMin(value = "0.1", message = "O valor mínimo para porcentagemLucro é 0.1")
    @DecimalMax(value = "100", message = "O valor máximo para porcentagemLucro é 100")
    private Double porcentagemLucro;

    public Investimento() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public RiscoInvestimento getRisco() {
        return risco;
    }

    public void setRisco(RiscoInvestimento risco) {
        this.risco = risco;
    }

    public Double getPorcentagemLucro() {
        return porcentagemLucro;
    }

    public void setPorcentagemLucro(Double porcentagemLucro) {
        this.porcentagemLucro = porcentagemLucro;
    }
}
