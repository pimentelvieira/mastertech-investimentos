package com.mastertech.investimentos.models;

public class ResultadoSimulacao {

    private double resultadoSimulacao;

    public ResultadoSimulacao() {
    }

    public ResultadoSimulacao(Double resultadoSimulacao) {
        this.resultadoSimulacao = resultadoSimulacao;
    }

    public double getResultadoSimulacao() {
        return resultadoSimulacao;
    }

    public void setResultadoSimulacao(Double resultadoSimulacao) {
        this.resultadoSimulacao = resultadoSimulacao;
    }
}
