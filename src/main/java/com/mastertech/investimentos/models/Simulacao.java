package com.mastertech.investimentos.models;

import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.NotNull;

public class Simulacao {

    @NotNull(message = "O campo investimentoId não pode ser nulo")
    private Integer investimentoId;

    @NotNull(message = "O campo mesesAplicacao não pode ser nulo")
    private Integer mesesAplicacao;

    @NotNull(message = "O campo dinheiroAplicado não pode ser nulo")
    @DecimalMin(value = "100", message = "O valor mínimo para dinheiroAplicado é 100")
    private Double dinheiroAplicado;

    public Simulacao() {
    }

    public Integer getInvestimentoId() {
        return investimentoId;
    }

    public void setInvestimentoId(Integer investimentoId) {
        this.investimentoId = investimentoId;
    }

    public Integer getMesesAplicacao() {
        return mesesAplicacao;
    }

    public void setMesesAplicacao(Integer mesesAplicacao) {
        this.mesesAplicacao = mesesAplicacao;
    }

    public Double getDinheiroAplicado() {
        return dinheiroAplicado;
    }

    public void setDinheiroAplicado(Double dinheiroAplicado) {
        this.dinheiroAplicado = dinheiroAplicado;
    }
}
