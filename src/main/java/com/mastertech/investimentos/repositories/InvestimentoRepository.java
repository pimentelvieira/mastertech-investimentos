package com.mastertech.investimentos.repositories;

import com.mastertech.investimentos.models.Investimento;
import org.springframework.data.repository.CrudRepository;

public interface InvestimentoRepository extends CrudRepository<Investimento, Integer> {
}
