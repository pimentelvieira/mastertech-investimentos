package com.mastertech.investimentos.services;

import com.mastertech.investimentos.models.Investimento;
import com.mastertech.investimentos.models.ResultadoSimulacao;
import com.mastertech.investimentos.models.Simulacao;
import com.mastertech.investimentos.repositories.InvestimentoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class InvestimentoService {

    @Autowired
    private InvestimentoRepository repository;

    public Investimento inserir(Investimento investimento) {
        return this.repository.save(investimento);
    }

    public Optional<Investimento> buscarPorId(Integer id) {
        return this.repository.findById(id);
    }

    public void deletar(Integer id) {
        this.repository.deleteById(id);
    }

    public Investimento atualizar(Investimento investimento) {
        return this.repository.save(investimento);
    }

    public List<Investimento> buscarTodos() {
        return (List) this.repository.findAll();
    }

    public ResultadoSimulacao simularInvestimento(Simulacao simulacao, Investimento investimento) {
        double resultadoSimulacao = simulacao.getDinheiroAplicado();

        for(int i = 0; i < simulacao.getMesesAplicacao(); i ++) {
            double porc = investimento.getPorcentagemLucro() / 100;
            double juros = resultadoSimulacao * porc;
            resultadoSimulacao = resultadoSimulacao + juros;
        }

        return new ResultadoSimulacao(resultadoSimulacao);
    }
}
