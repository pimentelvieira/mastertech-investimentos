package com.mastertech.investimentos.controllers;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.mastertech.investimentos.enums.RiscoInvestimento;
import com.mastertech.investimentos.models.Investimento;
import com.mastertech.investimentos.models.ResultadoSimulacao;
import com.mastertech.investimentos.models.Simulacao;
import com.mastertech.investimentos.services.InvestimentoService;
import org.hamcrest.CoreMatchers;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.util.Arrays;
import java.util.Optional;

@WebMvcTest
public class InvestimentoControllerTest {

    @MockBean
    private InvestimentoService service;

    @Autowired
    private MockMvc mockMvc;

    private Investimento investimento;

    private ObjectMapper objectMapper;

    @BeforeEach
    public void inicializar() {
        investimento = new Investimento();
        objectMapper = new ObjectMapper();

        investimento.setId(1);
        investimento.setNome("Itaú Poupança");
        investimento.setDescricao("Itaú Poupança");
        investimento.setPorcentagemLucro(10.0);
        investimento.setRisco(RiscoInvestimento.BAIXO);
    }

    @Test
    public void testaCadastroInvestimento() throws Exception {
        Mockito.when(service.inserir(Mockito.any(Investimento.class))).thenReturn(investimento);
        String json = objectMapper.writeValueAsString(investimento);
        mockMvc.perform(MockMvcRequestBuilders.post("/investimentos")
                .contentType(MediaType.APPLICATION_JSON)
                .content(json))
                .andExpect(MockMvcResultMatchers.status().isCreated())
                .andExpect(MockMvcResultMatchers.jsonPath("$.nome", CoreMatchers.equalTo(investimento.getNome())));
    }

    @Test
    public void testaBuscarPorId() throws Exception {
        Mockito.when(service.buscarPorId(Mockito.anyInt())).thenReturn(Optional.of(investimento));
        mockMvc.perform(MockMvcRequestBuilders.get("/investimentos/1"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.nome", CoreMatchers.equalTo(investimento.getNome())))
                .andExpect(MockMvcResultMatchers.jsonPath("$.descricao", CoreMatchers.equalTo(investimento.getDescricao())))
                .andExpect(MockMvcResultMatchers.jsonPath("$.risco", CoreMatchers.equalTo(investimento.getRisco().name())));
    }

    @Test
    public void testaBuscarPorIdNaoEncontrado() throws Exception {
        Mockito.when(service.buscarPorId(Mockito.anyInt())).thenReturn(Optional.empty());
        mockMvc.perform(MockMvcRequestBuilders.get("/investimentos/4"))
                .andExpect(MockMvcResultMatchers.status().isBadRequest());
    }

    @Test
    public void testaDeletarInvestimento() throws Exception {
        Mockito.when(service.buscarPorId(Mockito.anyInt())).thenReturn(Optional.of(investimento));
        mockMvc.perform(MockMvcRequestBuilders.delete("/investimentos/1"))
                .andExpect(MockMvcResultMatchers.status().isOk());
    }

    @Test
    public void testaDeletarInvestimentoNaoEncontrado() throws Exception {
        Mockito.when(service.buscarPorId(Mockito.anyInt())).thenReturn(Optional.empty());
        mockMvc.perform(MockMvcRequestBuilders.delete("/investimentos/4"))
                .andExpect(MockMvcResultMatchers.status().isBadRequest());
    }

    @Test
    public void testaBuscarTodos() throws Exception {
        Mockito.when(service.buscarTodos()).thenReturn(Arrays.asList(investimento));
        mockMvc.perform(MockMvcRequestBuilders.get("/investimentos"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].nome", CoreMatchers.equalTo(investimento.getNome())))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].descricao", CoreMatchers.equalTo(investimento.getDescricao())))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].risco", CoreMatchers.equalTo(investimento.getRisco().name())));
    }

    @Test
    public void testaAtualizarInvestimento() throws Exception {
        Investimento investimentoAtualizado = new Investimento();
        investimentoAtualizado.setId(1);
        investimentoAtualizado.setNome("Produto Atualizado");
        investimentoAtualizado.setDescricao("Descrição atualizada");
        investimentoAtualizado.setRisco(RiscoInvestimento.ALTO);
        investimentoAtualizado.setPorcentagemLucro(10.0);
        String json = objectMapper.writeValueAsString(investimentoAtualizado);
        Mockito.when(service.buscarPorId(Mockito.anyInt())).thenReturn(Optional.of(investimento));
        Mockito.when(service.atualizar(Mockito.any(Investimento.class))).thenReturn(investimentoAtualizado);
        mockMvc.perform(MockMvcRequestBuilders.put("/investimentos/1")
                .contentType(MediaType.APPLICATION_JSON)
                .content(json))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.nome", CoreMatchers.equalTo(investimentoAtualizado.getNome())))
                .andExpect(MockMvcResultMatchers.jsonPath("$.descricao", CoreMatchers.equalTo(investimentoAtualizado.getDescricao())))
                .andExpect(MockMvcResultMatchers.jsonPath("$.risco", CoreMatchers.equalTo(investimentoAtualizado.getRisco().name())));
    }

    @Test
    public void testaAtualizarInvestimentoNaoEncontrado() throws Exception {
        Investimento investimentoAtualizado = new Investimento();
        investimentoAtualizado.setId(1);
        investimentoAtualizado.setNome("Produto Atualizado");
        investimentoAtualizado.setDescricao("Descrição atualizada");
        investimentoAtualizado.setRisco(RiscoInvestimento.ALTO);
        String json = objectMapper.writeValueAsString(investimentoAtualizado);

        Mockito.when(service.buscarPorId(Mockito.anyInt())).thenReturn(Optional.empty());
        mockMvc.perform(MockMvcRequestBuilders.put("/investimentos/4")
                .contentType(MediaType.APPLICATION_JSON)
                .content(json))
                .andExpect(MockMvcResultMatchers.status().isBadRequest());
    }

    @Test
    public void testaCadastroInvestimentoComNomeNulo() throws Exception {
        investimento.setNome(null);
        Mockito.when(service.inserir(Mockito.any(Investimento.class))).thenReturn(investimento);
        String json = objectMapper.writeValueAsString(investimento);
        mockMvc.perform(MockMvcRequestBuilders.post("/investimentos")
                .contentType(MediaType.APPLICATION_JSON)
                .content(json))
                .andExpect(MockMvcResultMatchers.status().isBadRequest())
                .andExpect(MockMvcResultMatchers.jsonPath("$.erros[0].mensagem", CoreMatchers.equalTo("O campo nome não pode ser nulo")));
    }

    @Test
    public void testaCadastroInvestimentoComDescricaoNula() throws Exception {
        investimento.setDescricao(null);
        Mockito.when(service.inserir(Mockito.any(Investimento.class))).thenReturn(investimento);
        String json = objectMapper.writeValueAsString(investimento);
        mockMvc.perform(MockMvcRequestBuilders.post("/investimentos")
                .contentType(MediaType.APPLICATION_JSON)
                .content(json))
                .andExpect(MockMvcResultMatchers.status().isBadRequest())
                .andExpect(MockMvcResultMatchers.jsonPath("$.erros[0].mensagem", CoreMatchers.equalTo("O campo descrição não pode ser nulo")));
    }

    @Test
    public void testaCadastroInvestimentoComRiscoNulo() throws Exception {
        investimento.setRisco(null);
        Mockito.when(service.inserir(Mockito.any(Investimento.class))).thenReturn(investimento);
        String json = objectMapper.writeValueAsString(investimento);
        mockMvc.perform(MockMvcRequestBuilders.post("/investimentos")
                .contentType(MediaType.APPLICATION_JSON)
                .content(json))
                .andExpect(MockMvcResultMatchers.status().isBadRequest())
                .andExpect(MockMvcResultMatchers.jsonPath("$.erros[0].mensagem", CoreMatchers.equalTo("O campo risco não pode ser nulo")));
    }

    @Test
    public void testaCadastroInvestimentoComPorcentagemNula() throws Exception {
        investimento.setPorcentagemLucro(null);
        Mockito.when(service.inserir(Mockito.any(Investimento.class))).thenReturn(investimento);
        String json = objectMapper.writeValueAsString(investimento);
        mockMvc.perform(MockMvcRequestBuilders.post("/investimentos")
                .contentType(MediaType.APPLICATION_JSON)
                .content(json))
                .andExpect(MockMvcResultMatchers.status().isBadRequest())
                .andExpect(MockMvcResultMatchers.jsonPath("$.erros[0].mensagem", CoreMatchers.equalTo("O campo porcentagemLucro não pode ser nulo")));
    }

    @Test
    public void testaCadastroInvestimentoComPorcentagemAbaixoPermitido() throws Exception {
        investimento.setPorcentagemLucro(0.0);
        Mockito.when(service.inserir(Mockito.any(Investimento.class))).thenReturn(investimento);
        String json = objectMapper.writeValueAsString(investimento);
        mockMvc.perform(MockMvcRequestBuilders.post("/investimentos")
                .contentType(MediaType.APPLICATION_JSON)
                .content(json))
                .andExpect(MockMvcResultMatchers.status().isBadRequest())
                .andExpect(MockMvcResultMatchers.jsonPath("$.erros[0].mensagem", CoreMatchers.equalTo("O valor mínimo para porcentagemLucro é 0.1")));
    }

    @Test
    public void testaCadastroInvestimentoComPorcentagemAcimaPermitida() throws Exception {
        investimento.setPorcentagemLucro(100.1);
        Mockito.when(service.inserir(Mockito.any(Investimento.class))).thenReturn(investimento);
        String json = objectMapper.writeValueAsString(investimento);
        mockMvc.perform(MockMvcRequestBuilders.post("/investimentos")
                .contentType(MediaType.APPLICATION_JSON)
                .content(json))
                .andExpect(MockMvcResultMatchers.status().isBadRequest())
                .andExpect(MockMvcResultMatchers.jsonPath("$.erros[0].mensagem", CoreMatchers.equalTo("O valor máximo para porcentagemLucro é 100")));
    }

    public void testaSimularInvestimento() throws Exception {
        Simulacao simulacao = new Simulacao();
        simulacao.setInvestimentoId(1);
        simulacao.setDinheiroAplicado(1000.0);
        simulacao.setMesesAplicacao(10);
        Mockito.when(service.buscarPorId(Mockito.anyInt())).thenReturn(Optional.of(investimento));
        ResultadoSimulacao resultadoSimulacao = service.simularInvestimento(simulacao, investimento);
        String json = objectMapper.writeValueAsString(simulacao);
        mockMvc.perform(MockMvcRequestBuilders.put("/investimentos/simulacao")
                .contentType(MediaType.APPLICATION_JSON)
                .content(json))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.resultadoSimulacao", CoreMatchers.equalTo(resultadoSimulacao.getResultadoSimulacao())));
    }

    public void testaSimularInvestimentoNaoEncontrado() throws Exception {
        Simulacao simulacao = new Simulacao();
        simulacao.setInvestimentoId(2);
        simulacao.setDinheiroAplicado(1000.0);
        simulacao.setMesesAplicacao(10);
        Mockito.when(service.buscarPorId(Mockito.anyInt())).thenReturn(Optional.empty());
        ResultadoSimulacao resultadoSimulacao = service.simularInvestimento(simulacao, investimento);
        String json = objectMapper.writeValueAsString(simulacao);
        mockMvc.perform(MockMvcRequestBuilders.put("/investimentos/simulacao")
                .contentType(MediaType.APPLICATION_JSON)
                .content(json))
                .andExpect(MockMvcResultMatchers.status().isBadRequest());
    }

    public void testaSimularInvestimentoComIdNulo() throws Exception {
        Simulacao simulacao = new Simulacao();
        simulacao.setInvestimentoId(null);
        simulacao.setDinheiroAplicado(1000.0);
        simulacao.setMesesAplicacao(10);
        ResultadoSimulacao resultadoSimulacao = service.simularInvestimento(simulacao, investimento);
        String json = objectMapper.writeValueAsString(simulacao);
        mockMvc.perform(MockMvcRequestBuilders.put("/investimentos/simulacao")
                .contentType(MediaType.APPLICATION_JSON)
                .content(json))
                .andExpect(MockMvcResultMatchers.status().isBadRequest())
                .andExpect(MockMvcResultMatchers.jsonPath("$.erros[0].mensagem", CoreMatchers.equalTo("O campo investimentoId não pode ser nulo")));
    }

    public void testaSimularInvestimentoComMesesAplicacaoNulo() throws Exception {
        Simulacao simulacao = new Simulacao();
        simulacao.setInvestimentoId(1);
        simulacao.setDinheiroAplicado(1000.0);
        simulacao.setMesesAplicacao(null);
        ResultadoSimulacao resultadoSimulacao = service.simularInvestimento(simulacao, investimento);
        String json = objectMapper.writeValueAsString(simulacao);
        mockMvc.perform(MockMvcRequestBuilders.put("/investimentos/simulacao")
                .contentType(MediaType.APPLICATION_JSON)
                .content(json))
                .andExpect(MockMvcResultMatchers.status().isBadRequest())
                .andExpect(MockMvcResultMatchers.jsonPath("$.erros[0].mensagem", CoreMatchers.equalTo("O campo mesesAplicacao não pode ser nulo")));
    }

    public void testaSimularInvestimentoDinheiroAplicadoNulo() throws Exception {
        Simulacao simulacao = new Simulacao();
        simulacao.setInvestimentoId(1);
        simulacao.setDinheiroAplicado(null);
        simulacao.setMesesAplicacao(10);
        ResultadoSimulacao resultadoSimulacao = service.simularInvestimento(simulacao, investimento);
        String json = objectMapper.writeValueAsString(simulacao);
        mockMvc.perform(MockMvcRequestBuilders.put("/investimentos/simulacao")
                .contentType(MediaType.APPLICATION_JSON)
                .content(json))
                .andExpect(MockMvcResultMatchers.status().isBadRequest())
                .andExpect(MockMvcResultMatchers.jsonPath("$.erros[0].mensagem", CoreMatchers.equalTo("O campo dinheiroAplicado não pode ser nulo")));
    }

    public void testaSimularInvestimentoComDinheiroAplicadoMenorQueMinimo() throws Exception {
        Simulacao simulacao = new Simulacao();
        simulacao.setInvestimentoId(null);
        simulacao.setDinheiroAplicado(99.99);
        simulacao.setMesesAplicacao(10);
        ResultadoSimulacao resultadoSimulacao = service.simularInvestimento(simulacao, investimento);
        String json = objectMapper.writeValueAsString(simulacao);
        mockMvc.perform(MockMvcRequestBuilders.put("/investimentos/simulacao")
                .contentType(MediaType.APPLICATION_JSON)
                .content(json))
                .andExpect(MockMvcResultMatchers.status().isBadRequest())
                .andExpect(MockMvcResultMatchers.jsonPath("$.erros[0].mensagem", CoreMatchers.equalTo("O valor mínimo para dinheiroAplicado é 100")));
    }
}
