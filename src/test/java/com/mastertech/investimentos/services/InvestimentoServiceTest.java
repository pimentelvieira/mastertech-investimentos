package com.mastertech.investimentos.services;

import com.mastertech.investimentos.enums.RiscoInvestimento;
import com.mastertech.investimentos.models.Investimento;
import com.mastertech.investimentos.models.ResultadoSimulacao;
import com.mastertech.investimentos.models.Simulacao;
import com.mastertech.investimentos.repositories.InvestimentoRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

@SpringBootTest
public class InvestimentoServiceTest {

    @MockBean
    private InvestimentoRepository repository;

    @Autowired
    private InvestimentoService service;

    private Investimento investimento;

    @BeforeEach
    public void inicializar() {
        investimento = new Investimento();
        investimento.setId(1);
        investimento.setNome("Itaú Poupança");
        investimento.setDescricao("Itaú Poupança");
        investimento.setPorcentagemLucro(10.0);
        investimento.setRisco(RiscoInvestimento.BAIXO);
    }

    @Test
    public void testaSalvarInvestimento() {
        Mockito.when(repository.save(Mockito.any(Investimento.class))).thenReturn(investimento);
        Investimento investimentoResultado = service.inserir(investimento);

        Assertions.assertEquals(investimentoResultado, investimento);
        Assertions.assertEquals(investimentoResultado.getNome(), investimento.getNome());
    }

    @Test
    public void testaBuscaPorId() {
        Mockito.when(repository.findById(Mockito.anyInt())).thenReturn(Optional.of(investimento));

        Investimento investimentoResultado = service.buscarPorId(1).get();

        Assertions.assertEquals(investimentoResultado.getNome(), investimento.getNome());
        Assertions.assertEquals(investimentoResultado.getDescricao(), investimento.getDescricao());
        Assertions.assertEquals(investimentoResultado.getRisco(), investimento.getRisco());
    }

    @Test
    public void testaDeletarInvestimento() {
        service.deletar(1);
        Mockito.verify(repository).deleteById(Mockito.anyInt());
    }

    @Test
    public void testaAtualizarInvestimento() {
        Mockito.when(repository.save(Mockito.any(Investimento.class))).thenReturn(investimento);

        Investimento investimentoResultado = service.atualizar(investimento);

        Assertions.assertEquals(investimentoResultado, investimento);
        Assertions.assertEquals(investimentoResultado.getNome(), investimento.getNome());
        Assertions.assertEquals(investimentoResultado.getDescricao(), investimento.getDescricao());
        Assertions.assertEquals(investimentoResultado.getRisco(), investimento.getRisco());
    }

    @Test
    public void testaBuscarTodos() {
        Mockito.when(repository.findAll()).thenReturn(Arrays.asList(investimento));

        List<Investimento> investimentos = service.buscarTodos();

        Assertions.assertEquals(investimentos.get(0), investimento);
        Assertions.assertEquals(investimentos.get(0).getNome(), investimento.getNome());
        Assertions.assertEquals(investimentos.get(0).getDescricao(), investimento.getDescricao());
        Assertions.assertEquals(investimentos.get(0).getRisco(), investimento.getRisco());
    }

    @Test
    public void testaSimularInvestimento() {
        Simulacao simulacao = new Simulacao();
        simulacao.setInvestimentoId(1);
        simulacao.setDinheiroAplicado(1000.0);
        simulacao.setMesesAplicacao(10);

        ResultadoSimulacao resultadoSimulacao = service.simularInvestimento(simulacao, investimento);

        Assertions.assertEquals(resultadoSimulacao.getResultadoSimulacao(), Double.parseDouble("2593.7424601"));
    }
}
